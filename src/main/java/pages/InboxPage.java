package pages;


import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class InboxPage {

    private SelenideElement accountButton;
    private SelenideElement logoutButton;

    public void logout(){
        accountButton = $(By.xpath("//a[contains(@href,'SignOutOptions')]"));
        accountButton.should(Condition.visible);
        accountButton.click();

        logoutButton = $(By.xpath("//a[contains(@href,'Logout')]"));
        logoutButton.should(Condition.visible);
        logoutButton.click();

    }
}
