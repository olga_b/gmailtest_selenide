package pages;


import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;

public class LoginPage {

private SelenideElement emailField;

    public WelcomePage enterEmail(String email){
        emailField = $(By.cssSelector("input[type*='email']"));
        emailField.should(Condition.visible);
        emailField.setValue(email);
        $(By.xpath("//div[@id='identifierNext']")).click();

        return page(WelcomePage.class);

    }
}
