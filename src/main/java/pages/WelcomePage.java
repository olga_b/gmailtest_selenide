package pages;


import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;

public class WelcomePage {

    private SelenideElement passwordField;

    public InboxPage enterPassword(String password){
        passwordField = $(By.xpath("//input[@type='password']"));
        passwordField.should(Condition.visible);
        passwordField.setValue(password);
        $(By.xpath("//div[@id='passwordNext']")).click();

        return page(InboxPage.class);

    }
}
