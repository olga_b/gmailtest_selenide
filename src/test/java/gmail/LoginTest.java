package gmail;


import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import org.junit.Before;
import org.junit.Test;
import pages.InboxPage;
import pages.LoginPage;
import pages.WelcomePage;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

import static com.codeborne.selenide.Selenide.open;

public class LoginTest {
    protected Properties prop = new Properties();
    private File userProp = new File("user.properties");

    public LoginTest(){
        InputStreamReader fileInput = null;
        try {
            fileInput = new InputStreamReader(new FileInputStream(userProp), "UTF-8");
            prop.load(fileInput);
        } catch (IOException e) {
            System.out.println(("Property file not found"));
        }
    }

    @Before
    public void setup() {
        Configuration.baseUrl = "https://mail.google.com/";
    }

    @Test
    public void login() throws InterruptedException {
        LoginPage loginPage = open("https://mail.google.com/", LoginPage.class);
        WelcomePage welcomePage = loginPage.enterEmail(prop.getProperty("email"));
        welcomePage.enterPassword(prop.getProperty("password"));

        WebDriverRunner.closeWebDriver();

    }

    @Test
    public void logout() throws InterruptedException {
        LoginPage loginPage = open("/", LoginPage.class);
        WelcomePage welcomePage = loginPage.enterEmail(prop.getProperty("email"));
        InboxPage inboxPage = welcomePage.enterPassword(prop.getProperty("password"));
        inboxPage.logout();

        WebDriverRunner.closeWebDriver();

    }
}
